import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {
  user = this.userService.getUser().pipe(
    catchError(() => {
      const error = new Error('User Fetch Error');
      alert(error.message);
      return throwError(error);
    })
  );

  constructor(private userService: UserService) { }

}
