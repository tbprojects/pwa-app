import { Component } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

@Component({
  selector: 'app-updater',
  templateUrl: './updater.component.html',
  styleUrls: ['./updater.component.css']
})
export class UpdaterComponent {
  constructor(public updates: SwUpdate) {}

  update() {
    this.updates.activateUpdate().then(() => document.location.reload());
  }

}
